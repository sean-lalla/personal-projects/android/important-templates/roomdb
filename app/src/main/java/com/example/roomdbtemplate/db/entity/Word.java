package com.example.roomdbtemplate.db.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

//Defines this as a Word entity in the db.
//Table name is word_table
@Entity(tableName = "word_table")
public class Word {

//    @PrimaryKey(autoGenerate = true)
//            private int id;
//
//            @NonNull
//            private String word;
//            //..other fields, getters, setters

    //Forces Non-null Primary Key to be word
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "word")
    private String mWord;

    public Word(@NonNull String mWord) {
        this.mWord = mWord;
    }

    public String getWord() {
        return mWord;
    }
}
